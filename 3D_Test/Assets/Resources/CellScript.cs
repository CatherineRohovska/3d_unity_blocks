using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;
public class CellScript : MonoBehaviour {
public Text cellTxt;
public Button bttn1;
public Button bttn2;
	// Use this for initialization
    GameObject listLogic;
	void Start () {
	//cellTxt.text = "NoName";
        listLogic = GameObject.Find("MenuLogic");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void InitCell(string txt)
    {
        cellTxt.text = txt;    
        //Remove the existing events
    bttn1.onClick.RemoveAllListeners();
    //Add your new event using lambda notation
    bttn1.onClick.AddListener (AddObject);
        //
         bttn2.onClick.RemoveAllListeners();
    //Add your new event using lambda notation
    bttn2.onClick.AddListener (DeleteVariant);
        
    }
    void AddObject() {
    print(cellTxt.text);
        listLogic.GetComponent<MenuLogic>().LoadFromScroll(cellTxt.text);
     }
     void DeleteVariant()
    {
         if (EditorUtility.DisplayDialog("Deletion",
			"Do you want to delete model?", "Yes", "Cancel"))
        listLogic.GetComponent<MenuLogic>().DeleteElement(cellTxt.text);
    }
}
