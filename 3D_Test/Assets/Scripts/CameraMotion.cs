using UnityEngine;
using System.Collections;

public class CameraMotion : MonoBehaviour {
public float smooth = 2.0F;
    public float tiltAngle = 30.0F;
    float speed = 2;
	// Use this for initialization
    public float zoomSpeed = 10f;
public float minZoomFOV = 10f;
    public float maxZoomFOV = 50f;
    float minDist = 3f;
     float maxDist = 20f;
     Vector3 vectLook;
	void Start () {
	vectLook = new Vector3(5,0,5);
         transform.LookAt(vectLook);
	}
	
	// Update is called once per frame
	void Update () {
       
	float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;
    transform.RotateAround(vectLook, Vector3.up, tiltAroundZ * Time.deltaTime);
       if (Input.GetAxis("Vertical")>0) ZoomIn();
        if (Input.GetAxis("Vertical")<0) ZoomOut();
	}
    
    public void ZoomIn()
{
        float dist = Vector3.Distance( Camera.main.transform.position, vectLook);
        if (dist>minDist)
        Camera.main.transform.position = Camera.main.transform.position+Vector3.Normalize(transform.forward)/8;

        transform.LookAt(vectLook);
}
     public void ZoomOut()
     {
         float dist = Vector3.Distance( Camera.main.transform.position, vectLook);
         if (dist<maxDist)
         Camera.main.transform.position = Camera.main.transform.position-Vector3.Normalize(transform.forward)/8;

     }
   
}
