using UnityEngine;
using System.Collections;
 using System;
public class LevelInit : MonoBehaviour {
     public  int count = 6;
    public int posX = -1;
    public int posY = -1;
   public bool animation;
 public   float progress = 0;
  GameObject temp;
    GameObject blockGroup;
    GameObject centralCube;
   //
    Quaternion finalRotation;
    
	// Use this for initialization
	void Start () 
    {
        //
        animation = false;
        progress = 0;
        posX = -4;      
        posY = (-count/2); 
        blockGroup =  GameObject.Find("group");
        temp =  GameObject.Find("temp");
        count=6;
        for (int i =-count/2; i<=count/2; i++)
            for (int j = -count/2; j<=count/2; j++)
                for (int k = -count/2; k<=count/2; k++)
        {
            GameObject obj = Instantiate(Resources.Load("single_cube", typeof(GameObject))) as GameObject;
            if (i==j&& j==k) {centralCube = obj;}
            obj.transform.position =  new Vector3(i,j,k);
            obj.transform.parent = transform;
        }
        //
       // CheckCubesForWin();
    Shuffle2();
        //CheckCubesForWin();
	}
	//

	// Update is called once per frame
	void Update () 
    {
          if(Input.GetMouseButtonDown(0)&&!animation&&!blockGroup.GetComponent<groupScript>().selfAnimation) //Y
    {
       DeselectCubes();
              animation = true;
              progress = 0;
        posX = -4;      
        posY = -4;      
              print("Y");
  finalRotation  = transform.rotation* Quaternion.AngleAxis(90.0f, transform.InverseTransformDirection(Vector3.up));
              print(transform.up);
    }
              if(Input.GetMouseButtonDown(1)&&!animation&&!blockGroup.GetComponent<groupScript>().selfAnimation) //X
    {
                  
        print("X");
       DeselectCubes();
        posX = -4;      
        posY = -4;      
   //   transform.Rotate(new Vector3(1,0,0), 90, Space.Self); //vertical
   
             animation = true;
            finalRotation  = transform.rotation*Quaternion.AngleAxis(90, transform.InverseTransformDirection(Vector3.right));
                  print(transform.right);
    }
           if(Input.GetMouseButtonDown(2)&&!animation&&!blockGroup.GetComponent<groupScript>().selfAnimation)
    {
               print("Z");
       DeselectCubes();
        posX = -4;      
        posY = -4;      
        //transform.Rotate(new Vector3(0,0,1), 90, Space.Self); //vertical
        animation = true;
        finalRotation  = transform.rotation*Quaternion.AngleAxis(90, transform.InverseTransformDirection(Vector3.forward));

    }
        
             if(Input.GetKeyDown (KeyCode.LeftArrow)&&!blockGroup.GetComponent<groupScript>().selfAnimation)
    {
                 if (posX>-count/2)
                 {
                     posX--;
                 }
                 else
                     posX=-count/2;
                 SelectCubes(posX);
             
    }
        
               if(Input.GetKeyDown (KeyCode.RightArrow)&&!blockGroup.GetComponent<groupScript>().selfAnimation)
    {
                 if (posX<count/2)
                 {
                     posX++;
                 }
               SelectCubes(posX);
             
    }
	 if (animation==true) 
     {progress+=Time.deltaTime;
      //print(progress);
      if (progress>=1) {animation=false; progress=0;
                          transform.rotation = finalRotation;
                          CorrectCubes();
                        
                         }
      else
         transform.rotation = Quaternion.Slerp(transform.rotation, finalRotation, progress);  
     }
        
	}
    void SelectCubes(int position)
    {
        DeselectCubes();

        foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
       
         if ((int)Mathf.Round(cube.transform.position.x)==position)
         {
             cube.transform.parent = blockGroup.transform;
             cube.GetComponent<cubeScript>().SelectCube();
         }
    }
}
    void DeselectCubes()
    {
     
          foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
               cube.transform.parent = transform;
              cube.GetComponent<cubeScript>().DeselectCube();
          }
         blockGroup.transform.rotation=Quaternion.identity;
 
    }
 public  void CorrectCubes()
    {
        foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
        	Vector3 vect = cube.transform.position;
            //print(vect);
    vect.x = (vect.x*10)/10;//(float)Math.Round((double)vect.x,1);//Mathf.Floor(vect.x);    
    vect.y = (vect.y*10)/10;//(float)Math.Round((double)vect.y,1);//Mathf.Floor(vect.y);
    vect.z = (vect.z*10)/10;//(float)Math.Round((double)vect.z,1);//Mathf.Floor(vect.z);
    cube.transform.position = vect; 
           // print(vect);
   
    }
    }
    void Shuffle()
    {
        int rotationCount = 0;
        int selectColumn = 0;
        int randomRotation = 0;
        int rotationGroupCount = 0;
        Vector3 vectRotation = new Vector3(0,1,0);
        for (int i=0; i<50; i++)
        {
            randomRotation = UnityEngine.Random.Range(1,4);
            rotationCount = UnityEngine.Random.Range(1,4);
            rotationGroupCount = UnityEngine.Random.Range(1,4);
            if (randomRotation==0) {vectRotation = new Vector3(0,1,0);}
            if (randomRotation==1) {vectRotation = new Vector3(1,0,0);}
            if (randomRotation==2) {vectRotation = new Vector3(0,0,1);}
            transform.Rotate(vectRotation, 90*rotationCount, Space.Self); 
            selectColumn = UnityEngine.Random.Range(0,7);
            for (int j = 0; j<rotationGroupCount; j++)
            {
            SelectCubesWithoutColor(selectColumn);
            blockGroup.GetComponent<groupScript>().RotateGroupWithoutAnimation();
            DeselectCubesWithoutColor();   
            }
        }
    }
    void SelectCubesWithoutColor(int position)
    {
          foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
         
         if ((int)Mathf.Round(cube.transform.position.x)==position)
         {
             cube.transform.parent = blockGroup.transform;
         }
    }
    }
    void DeselectCubesWithoutColor()
    {
           foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
               cube.transform.parent = transform;
     }
         blockGroup.transform.rotation=Quaternion.identity;
    }
   public void CheckCubesForWin()
    {
       
        int k = GameObject.FindGameObjectsWithTag("cube").Length;
        int comparedCount = 0;
 
        print("Global: "+ Vector3.up+" "+ Vector3.right+" "+Vector3.forward);
           foreach (GameObject cube in GameObject.FindGameObjectsWithTag("cube"))
     {
               if (cube.transform.forward==centralCube.transform.forward && cube.transform.up==centralCube.transform.up && cube.transform.right==centralCube.transform.right) comparedCount++;
              //  print(transform.up+" "+ transform.right+" "+transform.forward);
     }
        if (k==comparedCount) print("Win");
    }
    void Shuffle2()
    {
            SelectCubesWithoutColor(2);
            blockGroup.GetComponent<groupScript>().RotateGroupWithoutAnimation();
            DeselectCubesWithoutColor();
    }
}