using UnityEngine;
using System.Collections;

public class cubeScript : MonoBehaviour {
private Color startcolor;
    GameObject blockGroup;
	// Use this for initialization
	void Start () {
	blockGroup =  GameObject.Find("group");
        startcolor = renderer.material.color;     
        GetComponent<MeshFilter>().mesh.uv =new Vector2[24]{ 
        new Vector2(2f/3f, 0.75f),
        new Vector2(1f/3f, 0.75f),
        new Vector2(2f/3f, 1f),
        new Vector2(1f/3f, 1f), 
            
        new Vector2(2f/3f, 0.25f),
        new Vector2(1f/3f, 0.25f),
        new Vector2(2f/3f, 0.5f),
        new Vector2(1f/3f, 0.5f),
            
        new Vector2(2/3f, 0f),
        new Vector2(1/3f, 0f),
        new Vector2(2/3f, 0.25f),
        new Vector2(1/3f, 0.25f),
            
        new Vector2(2/3f, 0.5f),
        new Vector2(1/3f, 0.75f),
        new Vector2(1/3f, 0.5f),
        new Vector2(2/3f, 0.75f),
            
        new Vector2(0f, 0f),
        new Vector2(1/3f, 0.25f),
        new Vector2(0f, 0.25f),
        new Vector2(1/3f, 0f),
            
        new Vector2(1f, 0.25f),
        new Vector2(2/3f, 0f),
        new Vector2(1f, 0f),
        new Vector2(2/3f, 0.25f)
                                                           };


	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseEnter()
 { 
  // print(transform.position);
 //  print((int)Mathf.Floor(transform.position.x));
 }
    

    public void DeselectCube()
    {
         renderer.material.color = startcolor;
    }
    public void SelectCube()
    {
        renderer.material.color = Color.red;
    }
}
