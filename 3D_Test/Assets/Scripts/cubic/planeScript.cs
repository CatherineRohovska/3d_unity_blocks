using UnityEngine;
using System.Collections;

public class planeScript : MonoBehaviour {
private Color startcolor;
	// Use this for initialization
	void Start () {
	 startcolor = renderer.material.color;
       // print(GetComponent<MeshFilter>().mesh.uv[0]+ " "+GetComponent<MeshFilter>().mesh.uv[1]+" "+ GetComponent<MeshFilter>().mesh.uv[2] + " "+GetComponent<MeshFilter>().mesh.uv.Length);
//print(GetComponent<MeshFilter>().mesh.normals[0]+" "+GetComponent<MeshFilter>().mesh.normals.Length);
         GetComponent<MeshFilter>().mesh.Clear();
        
        GetComponent<MeshFilter>().mesh.vertices = new Vector3[4]{new Vector3(5,0,5),new Vector3(-5,0,5),new Vector3(5,0,-5),new Vector3(-5,0,-5) };
        GetComponent<MeshFilter>().mesh.triangles = new int[6]{0,3,1,0,2,3};
         GetComponent<MeshFilter>().mesh.uv = new Vector2[4]{new Vector2(1f,1f),new Vector2(0f,1f),new Vector2(1f,0f),new Vector2(0f,0f) };
         GetComponent<MeshFilter>().mesh.normals = new Vector3[4]{new Vector3(0,1,0),new Vector3(0,1,0),new Vector3(0,1,0),new Vector3(0,1,0) };
//        print(GetComponent<MeshFilter>().mesh.normals[0]+" "+GetComponent<MeshFilter>().mesh.normals.Length);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Select()
    {
        renderer.material.color = Color.red;
    }
    public void Deselect()
    {
        renderer.material.color = startcolor;
    }
}
