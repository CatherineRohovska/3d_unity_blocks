using UnityEngine;
using System.Collections;

public class groupScript : MonoBehaviour {
bool globalAnimation;
public bool selfAnimation;
GameObject level;    
    float progress = 0;
     Quaternion finalRotation; 
	// Use this for initialization
	void Start () {
	level  = GameObject.Find("Level");
        selfAnimation=false;
        progress = 0;
	}
	
	// Update is called once per frame
	void Update () {
    globalAnimation = level.GetComponent<LevelInit>().animation;    
	    if(Input.GetKeyDown (KeyCode.Space)&&!globalAnimation&&!selfAnimation)
 {         selfAnimation =true;
            finalRotation  = transform.rotation*Quaternion.AngleAxis(90, transform.right);
        //  transform.Rotate(new Vector3(1,0,0), 90, Space.World); 
        
 }
        if (selfAnimation==true) 
     {progress+=Time.deltaTime;
     // print(progress);
      if (progress>=1) {selfAnimation=false; progress=0;
//                        finalRotation.x = Mathf.Floor(finalRotation.x);
//                        finalRotation.y = Mathf.Floor(finalRotation.y);
//                        finalRotation.z = Mathf.Floor(finalRotation.z);
                        transform.rotation = finalRotation;
                        level.GetComponent<LevelInit>().CorrectCubes();
                        level.GetComponent<LevelInit>().CheckCubesForWin();
                       }
  
      else
      {
         transform.rotation = Quaternion.Slerp(transform.rotation, finalRotation, progress);  
      }
     }
        
	}
    public void RotateGroupWithoutAnimation()
    {
        transform.Rotate(new Vector3(1,0,0), 90, Space.World); 
    }
}
