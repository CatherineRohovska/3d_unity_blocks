using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameScript : MonoBehaviour {
int playerTurn = 2;
public bool showAlert;
public GameObject alertCanvas;
public Text turn;
    string turnText = "Player turn: "; 
	// Use this for initialization
	void Start () {
	ChangeTurn();
        showAlert = false;
        alertCanvas = GameObject.Find("AlertWindow");
        alertCanvas.SetActive(false);
  
	}
	

	void Update () {

	}
public void ChangeTurn()
    {
    
        if (playerTurn == 1) 
        {
            playerTurn=2;
        }
        else
        {
            playerTurn=1;
        }
        turn.text = turnText + playerTurn;
    }
       
    public void ShowGameOver()
    {
       alertCanvas.SetActive(true);
        Time.timeScale = 0;
    }
 

      
   
}
