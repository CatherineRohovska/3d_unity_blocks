using UnityEngine;
using System.Collections;

public class LevelScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GenerateLevel();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void GenerateLevel()
    {
        int levels = Random.Range(7, 15);
        float starterY = 0.4f;
        int blocksPerLevel = Random.Range(2, 5);
        float starterX = Random.Range(2, 5);
        float height = 0;
        float width = 0;
        for (int i=0; i<levels; i++)
        {
            blocksPerLevel = Random.Range(2, 5);
           
                 GameObject block = Instantiate(Resources.Load("block", typeof(GameObject))) as GameObject;
             GameObject block2 = Instantiate(Resources.Load("block", typeof(GameObject))) as GameObject;
               
                 height = block.transform.localScale.y;
                 width = block.transform.localScale.x;
                 Vector3 point1 = new Vector3(5, starterY, 5);
                 Vector3 point2 = point1 + new Vector3(0,0,height);
                 Vector3 point3 = new Vector3(5-width/2, starterY, 5+height/2);
                 Vector3 point4 = new Vector3(5+width/2, starterY, 5+height/2);
             if (i%2==0) 
             {
                 block.transform.position = point1;
                 block2.transform.position = point2;
             }
            else
            {
                 block.transform.position = point3;
                 block2.transform.position = point4;
                block.transform.Rotate(Vector3.up, 90f);
                block2.transform.Rotate(Vector3.up, 90f);
            }
            starterY +=height;
            starterX = Random.Range(2, 5);
        }
    }
}
