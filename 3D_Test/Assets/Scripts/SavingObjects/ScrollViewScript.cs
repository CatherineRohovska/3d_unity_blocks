using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ScrollViewScript : MonoBehaviour {
GameObject contentLayer;
	// Use this for initialization
	void Start () {
	contentLayer = GameObject.Find("Backlayer");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void CreateList (List<string> list)
    {
        foreach (Transform child in contentLayer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
     contentLayer.GetComponent<RectTransform>().sizeDelta=new Vector2(contentLayer.GetComponent<RectTransform>().sizeDelta.x,45*list.Count);
        int i=0;
        Vector3 calcPos = new Vector3(0, 0, 0);
        foreach(string item in list)
        {
            i++;
            GameObject cell = Instantiate(Resources.Load("Cell", typeof(GameObject))) as GameObject;
            
            cell.transform.SetParent( contentLayer.transform, false);
            
            if (i==1)
            { //create first cell and start counting position
                calcPos = cell.GetComponent<RectTransform>().localPosition;
                //calcPos.y = cell.GetComponent<RectTransform>().sizeDelta.y;
                cell.GetComponent<RectTransform>().localPosition = calcPos;
            }
            else
            {
                calcPos.y-=cell.GetComponent<RectTransform>().sizeDelta.y;
                cell.GetComponent<RectTransform>().localPosition = calcPos;
            }
           // create new cell for table
            cell.GetComponent<CellScript>().InitCell(item);
        }
    }
}
